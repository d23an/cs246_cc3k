# cs246_cc3k

A game of CC3k consists of a board 79 columns wide and 30 rows high (5 rows are reserved for displaying information). Game play is as follows: the player character moves through a dungeon and slays enemies and collects treasure until reaching the end of the dungeon (where the end of the dungeon is the 5th floor). A dungeon consists of different floors that consist of chambers connected with passages. In our simplification, each floor will always consist of the same 5 chambers connected in exactly the same way (outlined below).

CC3k differs from other rogue-likes in a significant way: it does not update the terminal/window in real-time but rather redraws all elements every turn (e.g. after every command). Many early rogue-likes were programmed in a similar fashion to CC3k.

However, strange happenings are afoot in the chamber this term. The monsters have revolted and demanded to be made the heroes due to their negative portrayal in previous iterations of CC3k! Accordingly, we have acquiesced and so we have a new group of heroic adventurers this term.
